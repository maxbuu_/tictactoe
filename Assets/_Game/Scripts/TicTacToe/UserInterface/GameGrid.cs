using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TicTacToe.Gameplay;
using UnityEngine;
using UnityEngine.UI;
namespace TicTacToe.UserInterface
{
    public class GameGrid : MonoBehaviour
    {
        private const int GridWidth = 3;
        private const int GridHeight = 3;
        private const int CombinationCountToWin = 3;

        [SerializeField] private GameObject gameCellReference;
        [SerializeField] private Transform gameCellPanel;
        [SerializeField] private Vector2[] directionFactor;
        [SerializeField] private GridLayoutGroup gridLayoutGroup;
        public Dictionary<Vector2, GameCellController> Cells;

        private void Awake()
        {
            SetupGrid();
        }

        private IEnumerator Start()
        {
            yield return new WaitForEndOfFrame();
            gridLayoutGroup.enabled = false;
        }

        private void SetupGrid()
        {
            Cells = new Dictionary<Vector2, GameCellController>();
            int xCount = 0;
            int yCount = 0;

            for (int i = 0; i < GridHeight * GridWidth; i++)
            {
                Vector2 gridCoordinate = new Vector2(xCount, yCount);
                GameObject newCell = gameCellReference;

                if (i > 0)
                    newCell = Instantiate(gameCellReference, gameCellPanel);

                newCell.name = "GameCell_" + gridCoordinate;
                GameCellController gameCellModel = newCell.GetComponent<GameCellController>();
                gameCellModel.Setup(gridCoordinate);
                Cells.Add(gridCoordinate, gameCellModel);

                if (xCount + 1 < GridWidth)
                    xCount++;
                else
                {
                    xCount = 0;
                    yCount++;
                }
            }

            foreach (GameCellController cellsValue in Cells.Values)
            {
                cellsValue.SetupCombinations(GetAllCellCombination(cellsValue));
            }
        }

        private List<GameCellController[]> GetAllCellCombination(GameCellController gameCellController)
        {
            Vector2 cellCoordinate = gameCellController.model.coordinate;
            List<GameCellController[]> allPossibleCombinations = new List<GameCellController[]>();

            foreach (Vector2 dirFactor in directionFactor)
            {
                List<GameCellController> combination = new List<GameCellController> { gameCellController };

                for (int j = 1; j < CombinationCountToWin; j++)
                {
                    Vector2 inlinePositiveCell = dirFactor * 1 * j + cellCoordinate;
                    Vector2 inlineNegativeCell = dirFactor * -1 * j + cellCoordinate;

                    if (inlinePositiveCell.x < GridWidth && inlinePositiveCell.y < GridHeight && inlinePositiveCell.x >= 0 && inlinePositiveCell.y >= 0 && inlinePositiveCell != cellCoordinate)
                    {
                        combination.Add(Cells[inlinePositiveCell]);
                    }

                    if (inlineNegativeCell.x < GridWidth && inlineNegativeCell.y < GridHeight && inlineNegativeCell.x >= 0 && inlineNegativeCell.y >= 0 && inlinePositiveCell != cellCoordinate)
                    {
                        combination.Add(Cells[inlineNegativeCell]);
                    }
                }

                if (combination.Count < CombinationCountToWin)
                {
                    continue;
                }

                if (!allPossibleCombinations.Any(x => x.Equals(combination.ToArray())))
                    allPossibleCombinations.Add(combination.ToArray());
            }

            return allPossibleCombinations;
        }

        public void Clear()
        {
            foreach (GameCellController cell in Cells.Values)
            {
                cell.Clear();
            }
        }

        public GameCellController GetMiddleCell()
        {
            return Cells[new Vector2(1, 1)];
        }
    }
}