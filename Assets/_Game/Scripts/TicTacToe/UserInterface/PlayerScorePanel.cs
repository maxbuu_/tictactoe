using System;
using TicTacToe.Gameplay;
using UnityEngine;
using UnityEngine.UI;
namespace TicTacToe.UserInterface
{
    public class PlayerScorePanel : MonoBehaviour
    {
        [SerializeField] private Text figureText;
        [SerializeField] private Text scoreText;
        [SerializeField] private Image turnFeedback;
        private Player _player;
        
        public void SetupNewPlayer(Player player)
        {
            _player = player;
            figureText.text = _player.figure.TypeAsText;
            turnFeedback.enabled = player.isLocal;
            scoreText.text = _player.Score.ToString();
            RefreshFigureText();
        }

        public void RefreshPanel(Player playerTurn)
        {
            scoreText.text = _player.Score.ToString();
            turnFeedback.enabled = playerTurn.isLocal == _player.isLocal;
            RefreshFigureText();
        }

        private void RefreshFigureText()
        {
            string identificationText = "";
            identificationText = _player.isLocal ? "You" : "Computer";
            figureText.text = $"{_player.figure.TypeAsText} - {identificationText}";
        }
    }
}