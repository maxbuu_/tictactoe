using System;
using DG.Tweening;
using DG.Tweening.Core.Easing;
using UnityEngine;

namespace TicTacToe.Utility
{
    [Serializable]
    public struct MoveTweenStruct
    {
        public Vector3 position;
        public float duration;
        public Ease ease;
    }
}