using System;
using DG.Tweening;
using DG.Tweening.Core.Easing;
using UnityEngine;
namespace TicTacToe.Utility
{
    [Serializable]
    public struct ScaleTweenStruct 
    {
        public Vector2 startScale;
        public Vector2 endScale;
        public float duration;
        public Ease ease;
    }
}
