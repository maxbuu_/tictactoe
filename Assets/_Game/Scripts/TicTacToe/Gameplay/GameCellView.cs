﻿using DG.Tweening;
using TicTacToe.Utility;
using UnityEngine;
using UnityEngine.UI;
namespace TicTacToe.Gameplay
{
    public class GameCellView : MonoBehaviour
    {
        [SerializeField] private Text figureText;
        [SerializeField] private RectTransform _textTransform;

        [Header("Tween Structs")]
        [SerializeField] private ScaleTweenStruct figureSelectedTween;
        [SerializeField] private MoveTweenStruct endingMoveTween;
        [SerializeField] private ScaleTweenStruct endingScaleTween;
        [SerializeField] private Transform endingMovePositionReference;
        
        public void ChangeFigure(Figure newFigure)
        {
            figureText.text = newFigure.TypeAsText;
            _textTransform.localScale = figureSelectedTween.startScale;
            _textTransform.DOScale(figureSelectedTween.endScale, figureSelectedTween.duration).SetEase(figureSelectedTween.ease);
        }

        public void EndingAnimation()
        {
            _textTransform.SetParent(endingMovePositionReference);
            _textTransform.localScale = endingScaleTween.startScale;
            _textTransform.DOScale(endingScaleTween.endScale, endingScaleTween.duration).SetEase(endingScaleTween.ease);
            _textTransform.DOMove(endingMovePositionReference.position, endingMoveTween.duration).SetEase(endingMoveTween.ease);
        }

        public void ChangeTextColor(Color newColor)
        {
            figureText.color = newColor;
        }

        public void PunchTween()
        {
            _textTransform.DOPunchScale(new Vector2(1.25f, 1.25f), 0.2f, 2, 0.1f);
        }
    }
}