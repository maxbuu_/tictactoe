﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace TicTacToe.Gameplay
{
    public class GameCellController : MonoBehaviour
    {
        public GameCellModel model;
        [SerializeField] private GameCellView _view;

        private void Awake()
        {
            _view = GetComponent<GameCellView>();
        }

        public void SelectCell(Figure newFigure)
        {
            model.figure = newFigure;
            _view.ChangeFigure(newFigure);
            model.button.interactable = false;
        }

        public void Setup(Vector2 cellCoordinate)
        {
            model.button = GetComponent<Button>();
            model.coordinate = cellCoordinate;
            _view.ChangeFigure(model.figure);
            Clear();
        }

        public void SetupCombinations(List<GameCellController[]> combinationCells)
        {
            model.CombinationCells = combinationCells;
        }

        public void HighlightSequence(Color newColor)
        {
            _view.ChangeTextColor(newColor);
            _view.PunchTween();
        }

        public void Clear()
        {
            model.figure = new Figure
            {
                type = FigureType.None
            };

            model.button.interactable = true;
            _view.ChangeFigure(model.figure);
        }

        public void EndingComboAnimation()
        {
            _view.EndingAnimation();
        }

        public GameCellController[] FindSequenceCombination(int sequenceSize)
        {
            return model.FindSequenceCombination(sequenceSize);
        }

        public List<GameCellController[]> GetUnfinishedCellsSequence(int maximumEmpty, int sequenceSize)
        {
            return model.GetUnfinishedCellsSequence(maximumEmpty, sequenceSize);
        }
    }
}