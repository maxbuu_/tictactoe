﻿using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
namespace TicTacToe.Gameplay
{
    public class TicTacToeController : MonoBehaviour
    {
        [SerializeField] private TicTacToeModel model;
        private TicTacToeView _view;

        public Player EnemyPlayer => model.players.Single(x => !x.isLocal);
        public Player LocalPlayer => model.LocalPlayer;
        public GameCellController LastCellSelected => model.lastCellSelected;

        private void Awake()
        {
            _view = GetComponent<TicTacToeView>();
            Application.targetFrameRate = 60;
        }

        private void Start()
        {
            StarNewMatch();
        }
        
        private void OnApplicationQuit()
        {
            foreach (Player player in model.players)
            {
                player.ClearSave();
            }
        }

        public void StarNewMatch()
        {
            foreach (Player player in model.players)
            {
                player.Load();
            }

            model.matchState = MatchState.LocalPlayerTurn;
            model.onMatchStarted?.Invoke();
            _view.SetupPanel(model.players);
            _view.StartOpeningAnimation();
        }

        public void RestartGame()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        private void FinishMatch(Player winner, GameCellController[] cells)
        {
            model.matchState = MatchState.GameOver;
            model.onMatchOver?.Invoke();

            if (winner != null)
            {
                _view.RefreshPanel(winner);
                winner.Score++;
            }

            _view.StartEndingAnimation(cells, winner);
        }

        private void ChangeTurn()
        {
            model.CurrentPlayerIndex++;
            model.onTurnChanged?.Invoke(model.CurrentPlayer);
            _view.RefreshPanel(model.CurrentPlayer);
        }

        public void GameCellClicked(GameCellController cellChosen)
        {
            if (model.matchState != MatchState.LocalPlayerTurn || cellChosen.model.figure.type != FigureType.None)
            {
                return;
            }

            SelectGameCell(cellChosen);
        }

        public void SelectGameCell(GameCellController cellChosen)
        {
            model.matchState = model.CurrentPlayer.isLocal ? MatchState.EnemyTurn : MatchState.LocalPlayerTurn;
            cellChosen.SelectCell(model.CurrentPlayer.figure);
            model.lastCellSelected = cellChosen;

            if (!CheckVictory(cellChosen))
            {
                ChangeTurn();
            }
        }

        private bool CheckVictory(GameCellController cellChosen)
        {
            // Checking if there is a 3 pieces combo.
            GameCellController[] combo = cellChosen.model.FindSequenceCombination(3);

            if (combo == null)
            {
                //Tie Checking
                bool hasEmpty = false;

                foreach (GameCellController cell in model.gameGrid.Cells.Values)
                {
                    if (cell.model.figure.type == FigureType.None)
                        hasEmpty = true;
                }

                if (hasEmpty)
                    return false;
                
                FinishMatch(null, null);
                return true;

            }

            FinishMatch(model.CurrentPlayer, combo);
            return true;
        }

        public void SwapPlayers()
        {
            //In case the first play has already been made, it wont swap the players figure
            if (model.lastCellSelected != null)
                return;

            LocalPlayer.figure.type = FigureType.O;
            EnemyPlayer.figure.type = FigureType.X;

            ChangeTurn();
        }
    }
}