﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TicTacToe.UserInterface;
using TicTacToe.Utility;
using UnityEngine;
using UnityEngine.UI;
namespace TicTacToe.Gameplay
{
    public class TicTacToeView : MonoBehaviour
    {
        [SerializeField] private PlayerScorePanel[] scorePanels;
        [SerializeField] private float figureAnimationDelayBetween;
        [SerializeField] private Transform grid;

        [SerializeField] private ScaleTweenStruct gridEndingScaleTween;
        [SerializeField] private ScaleTweenStruct gridOpeningScaleTween;
        [SerializeField] private GameObject winnerCanvas;
        [SerializeField] private GameObject tieCanvas;
        
        [Header("Coloring Sequence")]
        [SerializeField] private float coloringDelay;
        [SerializeField] private Color figureSequenceColorLocal;
        [SerializeField] private Color figureSequenceColorEnemy;
        
        public void SetupPanel(Player[] players)
        {
            for (int i = 0; i < players.Length; i++)
            {
                // Debug.Log("Setup player " + players[i].figure.type);
                scorePanels[i].SetupNewPlayer(players[i]);
            }
        }

        public void RefreshPanel(Player playerTurn)
        {
            foreach (PlayerScorePanel panel in scorePanels)
            {
                panel.RefreshPanel(playerTurn);
            }
        }

        public void StartOpeningAnimation()
        {
            grid.localScale = gridOpeningScaleTween.startScale;
            grid.DOScale(gridOpeningScaleTween.endScale, gridOpeningScaleTween.duration).SetEase(gridOpeningScaleTween.ease);
        }

        public void StartEndingAnimation(IEnumerable<GameCellController> cells, Player winner)
        {
            StartCoroutine(winner != null ? WinnerAnimationRoutine(cells, winner) : TieAnimationRoutine());
        }

        private IEnumerator WinnerAnimationRoutine(IEnumerable<GameCellController> cells, Player winner)
        {
            foreach (GameCellController cell in cells)
            {
                cell.HighlightSequence(winner.isLocal ? figureSequenceColorLocal : figureSequenceColorEnemy);
                yield return new WaitForSeconds(coloringDelay);
            }

            yield return new WaitForSeconds(1);
            
            foreach (GameCellController gameCellController in cells)
            {
                gameCellController.EndingComboAnimation();
                yield return new WaitForSeconds(figureAnimationDelayBetween);
            }

            grid.localScale = gridEndingScaleTween.startScale;
            grid.DOScale(gridEndingScaleTween.endScale, gridEndingScaleTween.duration).SetEase(gridEndingScaleTween.ease);
            winnerCanvas.SetActive(true);
        }
        
        private IEnumerator TieAnimationRoutine()
        {
            grid.localScale = gridEndingScaleTween.startScale;
            grid.DOScale(gridEndingScaleTween.endScale, gridEndingScaleTween.duration).SetEase(gridEndingScaleTween.ease);
            yield return new WaitForSeconds(0.5f);
            tieCanvas.SetActive(true);
        }
    }
}