using System;
using UnityEngine;
namespace TicTacToe.Gameplay
{
    [Serializable]
    public class Figure
    {
        public FigureType type;
        public string TypeAsText  
        {
            get
            {
                string typeToString = "";

                if (type != FigureType.None)
                    typeToString = type.ToString();
                
                return typeToString;
            }
        }
    }
}