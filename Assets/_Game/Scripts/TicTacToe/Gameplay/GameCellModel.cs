using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
namespace TicTacToe.Gameplay
{
    [Serializable]
    public class GameCellModel
    {
        public Vector2 coordinate;
        public Button button;
        public Figure figure;
        public List<GameCellController[]> CombinationCells;

        //Theres might be a better way to do this
        public bool IsCornerCell => Math.Abs(coordinate.x - 1) > 0.1f && Math.Abs(coordinate.y - 1) > 0.1f;

        //Search for a sequence with the same figure
        public GameCellController[] FindSequenceCombination(int sequenceSize)
        {
            return CombinationCells.FirstOrDefault(gameCells => gameCells.Count(x => x.model.figure.type == figure.type) >= sequenceSize);
        }
        
        //Search for a sequence just like the method before, but also look for empty cells.
        //This method is being used by IA to find the best play
        public List<GameCellController[]> GetUnfinishedCellsSequence(int maximumEmptyCell, int sequenceSize)
        {
            List<GameCellController[]> gameCellCombinations = new List<GameCellController[]>(); 
            
            foreach (GameCellController[] gameCellSequence in CombinationCells)
            {
                int emptyCount = 0;
                int comboCount = 0;
                
                foreach (GameCellController cell in gameCellSequence)
                {
                    if (cell.model.figure.type == figure.type)
                    {
                        comboCount++;
                    }

                    if (cell.model.figure.type == FigureType.None)
                    {
                        emptyCount++;

                        if (emptyCount > maximumEmptyCell)
                        {
                            break;
                        }
                        
                        comboCount++;
                    }

                    if (comboCount >= sequenceSize)
                    {
                        gameCellCombinations.Add(gameCellSequence);
                    }
                }
            }

            return gameCellCombinations;
        }
    }

}