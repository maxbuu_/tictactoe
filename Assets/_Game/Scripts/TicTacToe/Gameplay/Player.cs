using System;
using UnityEngine;
namespace TicTacToe.Gameplay
{
    [Serializable]
    public class Player
    {
        private const string ScoreSaveId = "Score";
        public bool isLocal;
        public Figure figure;
        public int Score
        {
            set
            {
                _score = value;
                Save();
            }
            get => _score;
        }

        private int _score;
        public bool Equals(Player other)
        {
            return isLocal == other.isLocal && other.figure.type == figure.type;
        }

        public void Save()
        {
            PlayerPrefs.SetInt(ScoreSaveId + isLocal, Score);
        }

        public void Load()
        {
            Score = PlayerPrefs.GetInt(ScoreSaveId + isLocal);
        }

        public void ClearSave()
        {
            PlayerPrefs.DeleteKey(ScoreSaveId + isLocal);
        }
    }
}