using System;
using System.Linq;
using TicTacToe.UserInterface;
using UnityEngine;
using UnityEngine.Events;
namespace TicTacToe.Gameplay
{
    public enum MatchState { Waiting, LocalPlayerTurn, EnemyTurn, GameOver };
    
    [Serializable]
    public class TicTacToeModel 
    {
        public Player[] players;
        public Player LocalPlayer => players.SingleOrDefault(x => x.isLocal);
        public UnityEvent onMatchStarted;
        public UnityEvent onMatchOver;
        public MatchState matchState;
        public UnityEvent<Player> onTurnChanged;
        public Player CurrentPlayer => players[CurrentPlayerIndex];
        public int CurrentPlayerIndex
        {
            get => _currentPlayerIndex;
            set
            {
                _currentPlayerIndex = value;

                if (_currentPlayerIndex >= players.Length)
                    _currentPlayerIndex = 0;
            }
        }

        private int _currentPlayerIndex = 0;
        public GameCellController lastCellSelected;
        public GameGrid gameGrid;
    }
}
