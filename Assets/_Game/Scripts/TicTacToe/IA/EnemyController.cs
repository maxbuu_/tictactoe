using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TicTacToe.Gameplay;
using UnityEngine;
using Random = UnityEngine.Random;
namespace TicTacToe.IA
{
    public class EnemyController : MonoBehaviour
    {
        [SerializeField] private EnemyModel _model;
        private TicTacToeController _controller;
        private bool IsMistake => Random.Range(0f, 1f) > _model.difficultyChosen.value;

        private void Start()
        {
            _controller = FindObjectOfType<TicTacToeController>();
            _model.enemyPlayer = _controller.EnemyPlayer;
            _model.localPlayer = _controller.LocalPlayer;
            _model.dropdown.value = PlayerPrefs.GetInt("Difficulty");
            _model.difficultyChosen = _model.difficultiesData.SingleOrDefault(x => (int)x.type == _model.dropdown.value);
        }

        public void OnTurnChanged(Player turnPlayer)
        {
            if (turnPlayer.Equals(_model.enemyPlayer))
            {
                StartCoroutine(DoPlayRoutine());
            }
        }

        private IEnumerator DoPlayRoutine()
        {
            yield return new WaitForSeconds(Random.Range(_model.randomThinkingDuration.x, _model.randomThinkingDuration.y));
            DoPlay();
        }

        private void DoPlay()
        {
            //The IA Strategy is simple. If there is the player choose the corner, then it selects the middle cell.
            GameCellController middleCell = _model.gameGrid.GetMiddleCell();

            if (_controller.LastCellSelected != null)
                if (_controller.LastCellSelected.model.IsCornerCell && middleCell.model.figure.type == FigureType.None && !IsMistake)
                {
                    _controller.SelectGameCell(middleCell);
                    return;
                }

            GameCellController enemyCellController = FindLastEmptyCell(_model.enemyPlayer.figure.type);
            GameCellController playerCellController = FindLastEmptyCell(_model.localPlayer.figure.type);

            //If there is a sequence open, it play on it
            if (enemyCellController != null && !IsMistake)
            {
                _controller.SelectGameCell(enemyCellController);
                return;
            }

            //if the player is closing a sequence, it breaks it.
            if (playerCellController != null && !IsMistake)
            {
                _controller.SelectGameCell(playerCellController);
                return;
            }

            //In case the player choose the middle cell, the IA selects a corner.
            GameCellController randomCorner = GetRandomCornerCell();            
            
            if (randomCorner != null)
                if (middleCell.model.figure.type == _model.localPlayer.figure.type && !IsMistake)
                {
                    _controller.SelectGameCell(randomCorner);
                    return;
                }

            //If all this fail, it selects a random cell.
            _controller.SelectGameCell(FindEmptyRandomCell());
        }

        private GameCellController FindEmptyRandomCell()
        {
            GameCellController[] gameCellControllers = _model.gameGrid.Cells.Values.Where(x => x.model.figure.type == FigureType.None).ToArray();
            return gameCellControllers.Length <= 0 ? null : gameCellControllers[Random.Range(0, gameCellControllers.Length)];
        }

        private GameCellController GetRandomCornerCell()
        {
            GameCellController[] gameCellControllers = _model.gameGrid.Cells.Values.Where(x => x.model.figure.type == FigureType.None && x.model.IsCornerCell).ToArray();
            return gameCellControllers.Length <= 0 ? null : gameCellControllers[Random.Range(0, gameCellControllers.Length)];
        }

        //Search for the a sequence that has a single empty cell
        private GameCellController FindLastEmptyCell(FigureType figureType)
        {
            List<GameCellController[]> cellsCombined = new List<GameCellController[]>();

            foreach (GameCellController cell in _model.gameGrid.Cells.Values.Where(x => x.model.figure.type == figureType))
            {
                cellsCombined.AddRange(cell.GetUnfinishedCellsSequence(1, 3));
            }

            return cellsCombined.Count <= 0 ? null : cellsCombined[Random.Range(0, cellsCombined.Count)].SingleOrDefault(x => x.model.figure.type == FigureType.None);
        }

        public void ChangeDifficulty(int value)
        {
            _model.difficultyChosen = _model.difficultiesData.SingleOrDefault(x => (int)x.type == value);
            _controller.StarNewMatch();
            PlayerPrefs.SetInt("Difficulty", value);
        }
    }

}