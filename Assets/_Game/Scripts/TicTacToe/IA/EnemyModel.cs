﻿using System;
using TicTacToe.Gameplay;
using TicTacToe.UserInterface;
using UnityEngine;
using UnityEngine.UI;
namespace TicTacToe.IA
{
    [Serializable]
    public class EnemyModel
    {
        public Dropdown dropdown;
        public GameGrid gameGrid;
        public Difficulty difficultyChosen;
        public Difficulty[] difficultiesData;
        public Player enemyPlayer;
        public Player localPlayer;

        [Serializable]
        public struct Difficulty
        {
            public DifficultyType type;
            public float value;
        }
        
        public Vector2 randomThinkingDuration;
    }
}